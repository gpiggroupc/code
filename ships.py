#!/usr/bin/python
import json, time

traces = []
t = int(time.time())

for line in open('ship-positions.txt').readlines():
    if len(line.strip()) == 0:
        print json.dumps(traces)
        traces = []
    else:
        trace = dict(zip(['latitude', 'longitude', 'bearing', 'zone_id'], map(float, line.split())))
        trace['speed'] = 12.9 if trace['zone_id'] > 6 else 0
        trace['time'] = t
        trace['vessel_id'] = 220416000
        t += 12
        traces.append(trace)

function populateStatuses(statuses, target) {
    var itemTemplate = _.template($('#template_item').html());

    $.each(statuses, function () {
        $(target).append(itemTemplate({
                time: formattime('HH:MM', this.lastChecked),
                description: this.name + ': ' + this.description
            }));
    });
}

$('#map_canvas').on('ready', function (event, map) {
    $.getJSON('/analysis/api/zoneStatus/', function (status_data) {
        populateStatuses(status_data.data, 'div#zone_messages');
    });

    $.when(
        $.getJSON('/analysis/api/berthStatus/'),
        $.getJSON('/analysis/api/berths/'))
    .done(function (status_data, berth_data) {
        var berthTemplate = _.template($('#template_berth').html());

        // add berths to the map
        $.each(berth_data[0].data, function () {
            drawZone(map, this);
            $('div.berths').append(berthTemplate(this));
        });

        populateStatuses(status_data[0].data, 'div#berth_messages');
    });
});

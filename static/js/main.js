$(function() {
    function updateTime() {
        document.getElementById('time').innerText = formattime('yyyy-mm-dd HH:MM UTC');
        window.setTimeout(updateTime, 60000);
    }
    updateTime();

    function updateWarn() {
        $.getJSON('/analysis/api/warn/', function (reply) {
            if (reply.warn === 1) {
                $('#warn_container').slideDown();
            } else {
                $('#warn_container').slideUp();
            }
            window.setTimeout(updateWarn, 5000);
        });
    }
    updateWarn();

    function updateIncidents() {
        $.getJSON('/analysis/api/berths/', function (data) {
            var s = 'good';
            $.each(data.data, function () {
                if (this.lastStatus.statusType === 'Error')
                    s = 'error';
                else if (this.lastStatus.statusType === 'Warning' && s !== 'error')
                    s = 'warning';
            });
            var msg;
            switch (s) {
                case 'good': msg = 'Systems normal'; break;
                case 'warning': msg = 'System problem'; break;
                case 'error': msg = 'Major incident'; break;
            }
            $('#incident_header').html(msg);
            $('#incident_header')[0].className = s;
            window.setTimeout(updateIncidents, 2000);
        });
    }
    updateIncidents();

    var mapEle = document.getElementById('map_canvas');
    if (mapEle !== null) {
        google.maps.visualRefresh = true;
        var map = new google.maps.Map(mapEle, {
                zoom: 14,
                center: new google.maps.LatLng(51.456574, 0.345383),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                mapTypeControlOptions: {
                     mapTypeIds: [google.maps.MapTypeId.ROADMAP, google.maps.MapTypeId.SATELLITE]
                },
                streetViewControl: false
        });
        $('#map_canvas').trigger('ready', map);
    }
});

function status2Color(s) {
    switch (s) {
        case 'Good': return '2BE01B';
        case 'Warning': return 'E0E01B';
        default: return 'FF0000';
    }
}

function drawZone(map, zone) {
    var color = status2Color(zone.lastStatus.statusType);

    return new google.maps.Polygon({
        map: map,
        paths: $.map(zone.dimensions, function (point) {
            return new google.maps.LatLng(point[0], point[1])
        }),
        strokeColor: color,
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: color,
        fillOpacity: 0.35
    });
}

function drawVessel(map) {
    return new google.maps.Polyline({
        map: map,
        strokeOpacity: 0,
        icons: [{
            icon: {
                path: 'M 0,-1 0,1',
                strokeOpacity: 1,
                scale: 2
            },
            offset: '0',
            repeat: '20px'
        },
        {
            icon: {
                path: 'm -15.102004,34.015908 0,-49.287702 c 0,-3.408935 10.0153122,-14.405431 14.91414053,-14.345971 4.68934417,0.057 14.91414247,10.937036 14.91414247,14.345971 l 0,49.287702 c 0,2.982828 -9.9427619,3.977104 -14.91414247,3.977094 -4.86349963,1e-5 -14.80625953,-0.994266 -14.80625953,-3.977094 z',
                strokeWeight: 2,
                strokeColor: '#F00',
                fillColor: '#F00',
                fillOpacity: 1,
                scale: 0.25,
                zIndex: 2
            },
            offset: '100%'
        }],
        zIndex: 2
    });
}

function formattime(format, t) {
    function pad(n) { if (n < 10) n = '0' + n; return n; }

    t = t ? new Date(t * 1000) : new Date();

    return format.replace('dd', pad(t.getUTCDate())).
        replace('mm', pad(t.getUTCMonth())).
        replace('yyyy', t.getUTCFullYear()).
        replace('HH', pad(t.getUTCHours())).
        replace('MM', pad(t.getUTCMinutes()));
}

var demo_start = 1356998400;

$('#map_canvas').on('ready', function (event, map) {
    var slider;

    // Stores events sorted in time order for each object
    var timelines = {zones: {}, berths: {}, vessels: {}};

    // Handlers to update zone/berth polygons/info windows
    var handlers = {zones: {}, berths: {}};

    // Used for showing/hiding objects with checkboxes
    var polygons = {zones: [], berths: [], vessels: {}};

    // Info window templates
    var zone_template = _.template($('#template_zones').html());
    var vessel_template = _.template($('#template_vessels').html());

    /* Create a zone handler
     * Initialises the polygon/info window and returns an
     * update handler for the zone */
    function zone_handler(zone, type) {
        var polygon = drawZone(map, zone);
        var info = new google.maps.InfoWindow();
        polygon.addListener('click', function (event) {
            info.setPosition(event.latLng);
            info.open(polygon.getMap());
        });
        polygons[type].push(polygon);

        var last_status;
        return function(s) {
            if (last_status !== s) {
                var color = status2Color(s.statusType);
                info.setContent(zone_template(s));
                polygon.setOptions({ strokeColor: color, fillColor: color });
                last_status = s;
            }
        }
    }

    // Create slider
    $('#slider').slider({value: 0}).on('slide', function (event) {
        // Update zones and berth polygons
        $.each(['zones', 'berths'], function (i, type) {
            $.each(timelines[type], function (id, statuses) {
                handlers[type][id](_.find(statuses, function (s) {
                    return event.value >= s.lastChecked;
                }));
            });
        });

        // Update vessel traces
        $.each(timelines.vessels, function (id, traces) {
            polygons.vessels[id].setPath(_.chain(traces)
                .filter(function (trace) {
                    return trace.time <= event.value && trace.time >= event.value - 80;
                })
                .map(function (trace) {
                    return new google.maps.LatLng(trace.latitude, trace.longitude);
                })
                .value());
        });
    });

    slider = $('#slider').data('slider');
    slider.formater = function (value) {
        return value === slider.max ? 'LIVE' : formattime('HH:MM', value);
    };

    // Media button handlers
    (function () {
        var timer, speed = 4;
        var speeds = [-10, -5, -2, -1, 1, 2, 5, 10];

        function step() {
            var time = slider.getValue();
            time += speeds[speed];
            if (time > slider.max || time < slider.min) {
                $('#play').click(); // trigger a pause
            } else {
                slider.setValue(time);
                timer = setTimeout(step, 10);
            }
        }

        $('#backward').click(function (event) {
            if (speed > 0) {
                speed--;
                $('#speed').html('x' + speeds[speed]);
            }
        });
        $('#forward').click(function (event) {
            if (speed < speeds.length - 1) {
                speed++;
                $('#speed').html('x' + speeds[speed]);
            }
        });

        $('#play').click(function (event) {
            $(this).children('i').toggleClass('icon-pause');
            if (timer) {
                clearTimeout(timer);
                timer = null;
            } else {
                step();
            }
        });
    })();


    // Tickbox handlers, shows/hides polygons of the associated type
    $.each(polygons, function (type) {
        $('input#' + type).click(function (event) {
            var checked = $(this).is(':checked');
            $.each(polygons[type], function () {
                this.setVisible(checked);
            });
        });
    });

    // Load all the events into the timeline
    $.when(
        $.getJSON('/analysis/api/zones/'),
        $.getJSON('/analysis/api/berths/'),
        $.getJSON('/analysis/api/vessel/'))
    .done(function (zones, berths, vessels, zone_statuses,
            berth_statuses, vessel_traces) {
        var max_time = 0, max_zone = 0, max_berth = 0;

        // Create zone update handlers and timelines
        $.each(zones[0].data, function () {
            handlers.zones[this.zoneId] = zone_handler(this, 'zones');
            timelines.zones[this.zoneId] = [];
        });
        $.each(berths[0].data, function () {
            handlers.berths[this.berthId] = zone_handler(this, 'berths');
            timelines.berths[this.berthId] = [];
        });

        // Create vessel map objects and timelines
        $.each(vessels[0].data, function () {
            var line = drawVessel(map);
            var info = new google.maps.InfoWindow({
                content: vessel_template(this)
            });

            line.addListener('click', function (event) {
                info.setPosition(event.latLng);
                info.open(map);
            });

            // scale with zoom
            map.addListener('zoom_changed', function (event) {
                var ships = line.get('icons');
                ships[1].icon.scale = Math.max(0.25 * Math.pow(2, map.getZoom() - 15), 0.02);
                ships[0].icon.scale = Math.max(4 * Math.pow(2, map.getZoom() - 15), 0.4);
                ships[0].repeat = Math.max(20 * Math.pow(2, map.getZoom() - 15), 2) + 'px';
                line.set('icons', ships);
            });

            polygons.vessels[this.id] = line;
            timelines.vessels[this.id] = [];
        });

        function get_def(obj, key, def) {
            return obj ? obj[key] : def;
        }

        function refresh() {
            $.when(
                $.getJSON('/analysis/api/zoneStatus/?startTimestamp=' + (max_zone + 1)),
                $.getJSON('/analysis/api/berthStatus/?startTimestamp=' + (max_berth + 1)),
                $.getJSON('/analysis/api/vesselTrace/?startTimestamp=' + (max_time + 1)))
            .done(function (zone_statuses, berth_statuses, vessel_traces) {
                // Extract response data
                zone_statuses = zone_statuses[0].data.reverse();
                berth_statuses = berth_statuses[0].data.reverse();
                vessel_traces = vessel_traces[0].data.reverse();

                // Get min/max times, remember that data is ordered by time
                max_time = Math.max(demo_start + 1, max_time,
                    get_def(_.last(zone_statuses), 'lastChecked', max_time),
                    get_def(_.last(berth_statuses), 'lastChecked', max_time),
                    get_def(_.last(vessel_traces), 'time', max_time))

                max_zone = Math.max(max_zone, get_def(_.last(zone_statuses), 'lastChecked', max_zone))
                max_berth = Math.max(max_berth, get_def(_.last(berth_statuses), 'lastChecked', max_berth))

                // Add events to timelines
                $.each(zone_statuses, function () {
                    timelines.zones[this.laneZone_id].unshift(this);
                });
                $.each(berth_statuses, function () {
                    timelines.berths[this.berth_id].unshift(this);
                });
                $.each(vessel_traces, function () {
                    timelines.vessels[this.vessel_id].push(this);
                });

                var value = slider.getValue();
                if (value === slider.max || value === 0) {
                    value = max_time; // Slider is in LIVE position
                }
                slider.min = demo_start;
                slider.max = max_time;
                slider.setValue(value);
                slider.layout();

                $('#loading').fadeOut(1000);
                $('#controls').slideDown(1000);

                setTimeout(refresh, 500);
            });
        }
        refresh();
    });
});

0 WAIT Starting
5 TRACE 248781000 trace7.json 10 10

35 ZONE Warning 10 Broken buoy
36 WAIT Oh no, broken buoy

38 ZONE Good 10 Buoy fixed, fully operational

39 WARN blah
39 WAIT Show warning
39 NOWARN blah
39 WAIT Hide warning

40 TRACE 220416000 trace2.json 49 10
42 TRACE 227169690 trace6.json 52 58

59 WAIT First ship leaving

62 BERTH Warning 4 Operational problems
65 BERTH Error 4 Cannot release ship
70 WAIT Massive berth error

75 TRACE 304080890 trace3.json 85 10

95 ZONE Error 7 Man in the water
110 BERTH Good 4 Release mechanism fixed

140 ZONE Good 7 Fully operational

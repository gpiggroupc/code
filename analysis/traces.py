import json

print 'var data = ['
for line in open('traces.json'):
    traces = json.loads(line)
    for trace in traces:
        print '[%f, %f],' % (trace['latitude'], trace['longitude'])
print ']'

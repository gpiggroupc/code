#!/usr/bin/python
import sys, json, httplib, os
import time
from collections import defaultdict

def send(endpoint, payload):
    conn = httplib.HTTPConnection('localhost')
    conn.request('POST', '/analysis/api/%s/' % endpoint, json.dumps(payload))
    conn.getresponse().read()

start_time = 1356998400

##################### EVENT HANDLERS ########################

def zone_handler(event_time, data):
    def do():
        status_type, lane_zone, desc = data.split(None, 2)
        send('zoneStatus', {
                'statusType': status_type,
                'laneZone_id': lane_zone,
                'description': desc,
                'lastChecked': event_time
            })
    return [(event_time, do)]

def berth_handler(event_time, data):
    def do():
        status_type, berth, desc = data.split(None, 2)
        send('berthStatus', {
                'statusType': status_type,
                'berth_id': berth,
                'description': desc,
                'lastChecked': event_time
            })
    return [(event_time, do)]

def wait_handler(event_time, data):
    def do():
        print 'Waiting:', data,
        sys.stdin.readline()
    print 'Story:', data
    return [(event_time, do)]

def trace_handler(event_time, data):
    vessel, trace_file, wait_start, wait_time = data.split(None)

    wait_start = start_time + int(wait_start) * 60
    wait_time = int(wait_time) * 60

    def doer(do_time, trace):
        def do():
            trace['vessel_id'] = vessel
            trace['time'] = do_time
            send('vesselTrace', trace)
        return (do_time, do)
    traces = json.load(open(trace_file))
    events = []
    for trace in traces:
        events.append(doer(event_time, trace))
        event_time += 20
        if event_time > wait_start:
            while wait_time > 0:
                events.append(doer(event_time, trace))
                event_time += 20
                wait_time -= 20
    return events

def warn_handler(event_time, data):
    def do():
        open('warn', 'w').close()
    return [(event_time, do)]

def nowarn_handler(event_time, data):
    def do():
        os.remove('warn')
    return [(event_time, do)]

handlers = {
    'ZONE': zone_handler,
    'BERTH': berth_handler,
    'WAIT': wait_handler,
    'TRACE': trace_handler,
    'WARN': warn_handler,
    'NOWARN': nowarn_handler,
}

events = defaultdict(list)

# collect all the events
for line in open(sys.argv[1]):
    line = line.strip()
    if len(line) == 0 or line[0] == '#':
        continue

    offset, event_type, data = line.split(None, 2)

    for event_time, handler in handlers[event_type](start_time + int(offset) * 60, data):
        events[event_time].append(handler)

# run the events
while True:
    if start_time in events:
        for handler in events[start_time]:
            handler()

    time.sleep(0.01)
    start_time += 1

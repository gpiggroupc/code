from django.conf.urls import patterns, include, url
from Shimmer.rest_framework import ModelResource
import views

from django.contrib import admin
admin.autodiscover()

def model_urls(handler):
    return include(ModelResource(handler).urls)

apipatterns = patterns('',
    url(r'^vessel/', model_urls(views.VesselHandler)),
    url(r'^vesselDetails/', model_urls(views.VesselDetailsHandler)),
    url(r'^cargoType/', model_urls(views.CargoTypeHandler)),
    url(r'^zones/', model_urls(views.LaneZoneHandler)),
    url(r'^berths/', model_urls(views.BerthHandler)),
    url(r'^plan/', model_urls(views.PlanHandler)),
    url(r'^planItem/', model_urls(views.PlanItemHandler)),
    url(r'^planAlteration/', model_urls(views.PlanAlterationHandler)),
    url(r'^vesselTrace/', model_urls(views.MovementLogHandler)),
    url(r'^weather/', model_urls(views.WeatherHandler)),
    url(r'^shipModel/', model_urls(views.ShipModelHandler)),
    url(r'^zoneStatus/', model_urls(views.LaneZoneStatusHandler)),
    url(r'^berthStatus/', model_urls(views.BerthStatusHandler)),
    url(r'^incident/', model_urls(views.IncidentHandler)),
    url(r'^shippingLane/', model_urls(views.ShippingLaneHandler)),
    url(r'^mooringHistory/', model_urls(views.MooringLogHandler)),
    url(r'^query/', 'analysis.views.query'),
    url(r'^warn/', 'analysis.views.warn')
)

urlpatterns = patterns('analysis.views',
   url(r'^$', 'overview'),
   url(r'^status/$', 'status'),
   url(r'^incidents/$', 'incidents'),
   url(r'^analysis/$', 'analysis'),
   url(r'^report/$', 'report'),
   url(r'^api/', include(apipatterns)),

   url(r'^admin/', include(admin.site.urls)),
)

from django.db import models

class Vessel(models.Model):
    """
        Defines a vessel within the system

        A vessel is identified by a 9-digit MMSI and has a model of ship
        associated with it.
    """
    id = models.IntegerField(primary_key=True) # Just in case this needs to be
                                              # adapted later, it's MMSI
    name = models.CharField(max_length=150)
    callsign = models.CharField(max_length=50)
    vesselType = models.ForeignKey('ShipModel')

    include_fields = ('shipModel','plannedRoute',)

    @property
    def shipModel(self):
        return [self.vesselType]

    @property
    def plannedRoute(self):
        """
            Get the planned route for this vessel if one currently exists.

            This extracts all of the PlanItem entries relating to this vessel.

            @return All PlanItems which reference this vessel
        """
        return PlanItem.objects.filter(vessel=self)

class ShipModel(models.Model):
   """
      A model/type of ship.

      Each vessel is a particular model which imposes certain constraints on the
      vessel in question. Information such as the maximum capacity and which
      types of cargo the vessel can carry are recorded in this class.
   """
   name = models.CharField(max_length=100)
   maxContainers = models.IntegerField()
   maxVolume = models.IntegerField()
   beam = models.FloatField()
   height = models.FloatField()
   length = models.FloatField()
   draught = models.FloatField()
   canCarry = models.ManyToManyField('CargoType')

class Cargo(models.Model):
   """
      A particular set of cargo

      This object is an abstract model which serves the creation of the
      ContainerCargo and BulkCargo objects
   """
   vessel = models.ForeignKey('VesselDetails')
   cargoType = models.ForeignKey('CargoType')

class ContainerCargo(Cargo):
   """
      Cargo in containers of a particular size

      The container size value is measured in TEU
   """
   containerQuantity = models.IntegerField()
   containerSize = models.IntegerField(default=1)

class BulkCargo(Cargo):
   """
      Cargo transported in bulk like liquids, grain, etc
   """
   volume = models.IntegerField()

class CargoType(models.Model):
   """
      A type of cargo

      These are in a tree structure, allowing for grouping of cargo items as
      long as multiple inheritance isn't desired
   """
   parentType = models.ForeignKey('CargoType', null=True)
   name = models.CharField(max_length=100)

class Plan(models.Model):
    """
        Defines a plan comprising many plan items

        The plan has a defined start and end time, and includes a contingency
        period to make planning happy.
    """
    contingency = models.IntegerField()
    planStart = models.DateTimeField()
    planEnd = models.DateTimeField()

    include_fields = ('timetable','alterations',)

    @property
    def timetable(self):
        return PlanItem.objects.filter(plan=self)

    @property
    def alterations(self):
        ret = []
        for a in PlanAlteration.objects.all():
            if a.planItem.plan.id == self.id:
                ret.append(a)
        return ret

class PlanItem(models.Model):
   """
      A planned time at which a particular vessel should have reached a lane
      zone

      This is not to be confused with a deadline, which is a hard worst-case
      limit, this is an aim, and it might be missed without becoming a problem
   """
   plan = models.ForeignKey('Plan')
   time = models.DateTimeField()
   vessel = models.ForeignKey('Vessel')
   zone = models.ForeignKey('Zone')

class PlanAlteration(models.Model):
    C = 'Create'
    U = 'Update'
    D = 'Delete'

    planItem = models.ForeignKey('PlanItem')
    updateType = models.CharField(max_length=10)
    time = models.DateTimeField()
    vessel = models.ForeignKey('Vessel')
    zone = models.ForeignKey('LaneZone')
    reason = models.TextField()

class Deadline(models.Model):
   """lanes = models.ManyToManyField('ShippingLane')
      The last possible time at which a vessel should reach a particular lane
      zone

      This is a hard limit, and if a vessel fails to meet this deadline then a
      notice should be triggered as a result
   """
   vessel = models.ForeignKey('VesselDetails')
   laneZone = models.ForeignKey('LaneZone')
   time = models.DateTimeField()

class Position(models.Model):
   """
      A 2-dimensional cartesian coordinate (lat,lng)
   """
   lat = models.FloatField()
   lng = models.FloatField()

class ZonePosition(Position):
    """
    """
    zone = models.ForeignKey('Zone')

class MovementLog(models.Model):
    """
        A retained log of when vessels actually reached lane zones and how fast
        they were going at the time
    """
    vessel = models.ForeignKey('Vessel')
    time = models.DateTimeField()
    position = models.ForeignKey('Position')
    zone = models.ForeignKey('Zone')
    bearing = models.FloatField()
    speed = models.FloatField()

    include_fields = ('latitude','longitude',)

    @property
    def latitude(self):
        return self.position.lat

    @property
    def longitude(self):
        return self.position.lng

class Zone(models.Model):
    """
        Contains a list of positions
    """
    pass

class LaneZone(Zone):
    """
      A defined polygon with a depth which describes an area of water in the
      port
    """
    zoneId = models.AutoField(primary_key=True)
    depth = models.FloatField()
    zoneName = models.CharField(max_length=40)
    lanes = models.ManyToManyField('ShippingLane')

    include_fields = ('dimensions','history','lastStatus')

    @property
    def dimensions(self):
        positions = ZonePosition.objects.filter(zone=self)
        return [(pos.lat, pos.lng) for pos in positions]

    @property
    def history(self):
        return MovementLog.objects.filter(zone=self)

    @property
    def lastStatus(self):
        try:
            return LaneZoneStatus.objects.filter(laneZone=self).order_by('-lastChecked')[0]
        except IndexError:
            return []

class Berth(Zone):
    berthId = models.AutoField(primary_key=True)
    berthName = models.CharField(max_length=100)
    acceptedTypes = models.ManyToManyField('CargoType')

    include_fields = ('dimensions','lastStatus',)

    @property
    def dimensions(self):
        positions = ZonePosition.objects.filter(zone=self)
        return [(pos.lat, pos.lng) for pos in positions]

    @property
    def lastStatus(self):
        try:
            return BerthStatus.objects.filter(berth=self).order_by('-lastChecked')[0]
        except IndexError:
            return []

class ShippingLane(models.Model):
   """
      This is a placeholder for a shipping lane
   """
   include_fields = ('zones',)

   @property
   def zones(self):
       return LaneZone.objects.filter(lanes=self)

class Incident(models.Model):
   """
      An incident comprises a description, when it started, when it ended (if it
      has), where it occurred and which vessels it included
   """
   description = models.TextField()
   start = models.DateTimeField()
   end = models.DateTimeField()
   zonesInvolved = models.ManyToManyField('LaneZone')
   vesselsInvolved = models.ManyToManyField('Vessel')
   severity = models.IntegerField()

class WeatherConditions(models.Model):
   time = models.DateTimeField()
   zone = models.ForeignKey('LaneZone')

class Weather(models.Model):
   conditions = models.ForeignKey('WeatherConditions')
   windSpeed = models.FloatField()
   precipitationDepth = models.FloatField()

class Status(models.Model):
    lastChecked = models.DateTimeField()
    statusType = models.CharField(max_length=50)
    description = models.TextField()

class LaneZoneStatus(Status):
    laneZone = models.ForeignKey('LaneZone')

    include_fields = ('name',)

    @property
    def name(self):
        return self.laneZone.zoneName;

class BerthStatus(Status):
    berth = models.ForeignKey('Berth')

    include_fields = ('name',)

    @property
    def name(self):
        return self.berth.berthName;

# This is at the end because otherwise it break the fixture
class VesselDetails(models.Model):
    vessel = models.ForeignKey('Vessel')
    priority = models.IntegerField()
    turnaround = models.IntegerField()

    include_fields = ('cargo','deadlines',)

    @property
    def cargo(self):
        return Cargo.objects.filter(vessel=self)

    @property
    def deadlines(self):
        return Deadline.objects.filter(vessel=self)

class MooringLog(models.Model):
    vessel = models.ForeignKey('Vessel')
    berth = models.ForeignKey('Berth')
    arrived = models.DateTimeField()
    departed = models.DateTimeField()

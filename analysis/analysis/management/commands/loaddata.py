import json, random, time
from datetime import datetime, date, timedelta, tzinfo
from django.core.management.base import BaseCommand
from analysis.models import *

class UTC(tzinfo):
    def utcoffset(self, dt):
        return timedelta(0)

    def tzname(self, dt):
        return "UTC"

    def dst(self, dt):
        return timedelta(0)

utc = UTC()

def starttime():
    today = date.today()
    return datetime(2013, 1, 1, 0, 0, tzinfo=utc)

def randtime():
    return starttime() + timedelta(minutes=random.randint(0, 59))

def save_dimensions(z, dims):
    for point in dims:
        ZonePosition(zone=z, lat=point[0], lng=point[1]).save()

class Command(BaseCommand):
    def handle(self, *args, **options):
        self.stdout.write('Loading berths...')
        data = json.loads(open('berths.json').read())
        for berth in data:
            b = Berth(berthName=berth['berthName'])
            b.save()
            save_dimensions(b, berth['dimensions'])

        self.stdout.write('Loading zones...')
        data = json.loads(open('zones.json').read())
        for zone in data:
            l = LaneZone(depth=5.0, zoneName=zone['zoneName'])
            l.save()
            save_dimensions(l, zone['dimensions'])

        self.stdout.write('Loading test data...')
        sailing = ShipModel(
            name="Sailing",
            maxContainers=0,
            maxVolume=2000,
            beam=1,
            height=2,
            length=2,
            draught=4)
        sailing.save()

        container = ShipModel(
            name="Container",
            maxContainers=500,
            maxVolume=10000,
            beam=1,
            height=2,
            length=2,
            draught=4)
        container.save()

        bulk = ShipModel(
            name="Bulk",
            maxContainers=0,
            maxVolume=80000,
            beam=1,
            height=2,
            length=2,
            draught=4)
        bulk.save()

        bulkContainer = ShipModel(
            name="BulkContainer",
            maxContainers=4000,
            maxVolume=0,
            beam=1,
            height=2,
            length=2,
            draught=4)
        bulkContainer.save()

        georg = Vessel(
            id=220416000,
            name="GEORG MAERSK",
            callsign="OYGN2",
            vesselType=sailing)
        georg.save()

        isabelle = Vessel(
            id=227169690,
            name="ISABELLE CELINE",
            callsign="FAA9166",
            vesselType=bulk)
        isabelle.save()

        pauline = Vessel(
            id=304080890,
            name="PAULINE RUSS",
            callsign="V2OF",
            vesselType=bulkContainer)
        pauline.save()

        ship1 = Vessel(
            id=636091221,
            name="MAESRK SAIGON",
            callsign="A8KW3",
            vesselType=bulkContainer)
        ship1.save()

        ship2 = Vessel(
            id=248781000,
            name="CITY OF LUTECE",
            callsign="9HRJ6",
            vesselType=bulkContainer)
        ship2.save()

        ship3 = Vessel(
            id=103,
            name="Ship 3",
            callsign="sh3",
            vesselType=bulkContainer)
        ship3.save()

        ship4 = Vessel(
            id=104,
            name="Ship 4",
            callsign="sh4",
            vesselType=bulkContainer)
        ship4.save()

        ship5 = Vessel(
            id=105,
            name="Ship 5",
            callsign="sh5",
            vesselType=bulkContainer)
        ship5.save()

        ship6 = Vessel(
            id=106,
            name="Ship 6",
            callsign="sh6",
            vesselType=bulkContainer)
        ship6.save()

        ship7 = Vessel(
            id=107,
            name="Ship 7",
            callsign="sh7",
            vesselType=bulkContainer)
        ship7.save()

        georg_details = VesselDetails(
            vessel = georg,
            priority = 2,
            turnaround = 10)
        georg_details.save();

        isabelle_details = VesselDetails(
            vessel = isabelle,
            priority = 5,
            turnaround = 25)
        isabelle_details.save();

        pauline_details = VesselDetails(
            vessel = pauline,
            priority = 10,
            turnaround = 60)
        pauline_details.save();

        bulkCargo = CargoType(name="Bulk")
        bulkCargo.save()

        bulk.canCarry.add(bulkCargo)
        bulkContainer.canCarry.add(bulkCargo)

        containerCargo = CargoType(name="Container")
        containerCargo.save()

        container.canCarry.add(containerCargo)
        bulkContainer.canCarry.add(containerCargo)

        food = CargoType(parentType=containerCargo, name="Food")
        food.save()

        fruit = CargoType(parentType=food, name="Fruit")
        fruit.save()

        apples = CargoType(parentType=fruit, name="Apples")
        apples.save()

        pears = CargoType(parentType=fruit, name="Pears")
        pears.save()

        electronics = CargoType(parentType=containerCargo, name="Electronics")
        electronics.save()

        tvs = CargoType(parentType=electronics, name="Televisions")
        tvs.save()

        fuel = CargoType(parentType=bulkCargo, name="Fuel")
        fuel.save()

        petrol = CargoType(parentType=fuel, name="Petrol")
        petrol.save()

        diesel = CargoType(parentType=fuel, name="Diesel")
        diesel.save()

        # Add some cargo to the vessels
        georg_petrol = BulkCargo(
            vessel = georg_details,
            cargoType = petrol,
            volume = 1800)
        georg_petrol.save()

        isabelle_apples = ContainerCargo(
            vessel = isabelle_details,
            cargoType = apples,
            containerQuantity = 50)
        isabelle_apples.save()

        isabelle_pears = ContainerCargo(
            vessel = isabelle_details,
            cargoType = pears,
            containerQuantity = 50)
        isabelle_pears.save()

        pauline_tvs = ContainerCargo(
            vessel = pauline_details,
            cargoType = tvs,
            containerQuantity = 1750,
            containerSize = 2)
        pauline_tvs.save()

        # A shipping lane
        shippingLane = ShippingLane()
        shippingLane.save()

        # Berths are defined by a fixture
        b1 = Berth.objects.get(berthId=1)
        b2 = Berth.objects.get(berthId=2)
        b3 = Berth.objects.get(berthId=3)
        b4 = Berth.objects.get(berthId=4)
        b5 = Berth.objects.get(berthId=5)
        b6 = Berth.objects.get(berthId=6)

        b1.acceptedTypes.add(bulkCargo)
        b2.acceptedTypes.add(bulkCargo)
        b3.acceptedTypes.add(bulkCargo, containerCargo)
        b4.acceptedTypes.add(containerCargo)
        b5.acceptedTypes.add(bulkCargo, containerCargo)

        # LaneZones are defined by a fixture, get a few
        lz1 = LaneZone.objects.get(zoneId=1)
        lz2 = LaneZone.objects.get(zoneId=2)
        lz3 = LaneZone.objects.get(zoneId=3)
        lz4 = LaneZone.objects.get(zoneId=4)

        lz1.lanes.add(shippingLane)
        lz2.lanes.add(shippingLane)

        # Make a plan
        today = date.today()
        tomorrow = today + timedelta(days=1)
        myPlan = Plan(
            contingency = 20,
            planStart = datetime(today.year, today.month, today.day),
            planEnd = datetime(tomorrow.year, tomorrow.month, tomorrow.day))
        myPlan.save()

        # Put some plan items in
        #georg_plan1 = PlanItem(
            #plan=myPlan,
            #time=datetime(today.year, today.month, today.day, 8, 30),
            #vessel=georg,
            #zone=lz1)
        #georg_plan1.save()

        #georg_plan2 = PlanItem(
            #plan=myPlan,
            #time=datetime(today.year, today.month, today.day, 12, 30),
            #vessel=georg,
            #zone=lz2)
        #georg_plan2.save()

        # Have a plan alteration
        #georg_plan_alteration1 = PlanAlteration(
            #planItem = georg_plan2,
            #updateType = PlanAlteration.U,
            #time = datetime(today.year, today.month, today.day, 13),
            #vessel = georg,
            #zone = lz2,
            #reason = "Shifted backwards due to delays"
        #)
        #georg_plan_alteration1.save()

        # A deadline or two
        georg_deadline = Deadline(
            vessel = georg_details,
            laneZone = lz2,
            time = datetime(today.year, today.month, today.day, 9))
        georg_deadline.save()
        georg_deadline2 = Deadline(
            vessel = georg_details,
            laneZone = lz3,
            time = datetime(today.year, today.month, today.day, 11))
        georg_deadline2.save()

        # A set of weather conditions
        weatherConditions = WeatherConditions(
            time = datetime(today.year, today.month, today.day, 6),
            zone = lz1)
        weatherConditions.save()

        # Some weather
        weather = Weather(
            conditions = weatherConditions,
            windSpeed = 18,
            precipitationDepth = 1.21)
        weather.save()

        # have some initial statuses
        for lane in LaneZone.objects.all():
            LaneZoneStatus(
                lastChecked = starttime(),
                statusType = 'Good',
                description = 'Systems normal',
                laneZone = lane).save()
        for b in Berth.objects.all():
            BerthStatus(
                lastChecked = starttime(),
                statusType = 'Good',
                description = 'Systems normal',
                berth = b).save()

        # Mooring log
        mooring = MooringLog(
            vessel = georg,
            berth = b1,
            arrived = datetime(today.year, today.month, today.day, 14),
            departed = datetime(today.year, today.month, today.day, 16))
        mooring.save()

        # Oh no! An incident
        incident = Incident(
            description = "Two ships sailed into each other. Lawyers already on the scene",
            start = datetime(today.year, today.month, today.day, 12),
            end = datetime(today.year, today.month, today.day, 18),
            severity = 1)
        incident.save()
        incident.zonesInvolved.add(lz1)
        incident.vesselsInvolved.add(georg, pauline)

        """
        future = open('future_traces.json', 'w')
        self.stdout.write('Loading traces...')
        for line, vessel in zip(open('traces.json').readlines(), Vessel.objects.all()):
            traces = json.loads(line)
            t = randtime()

            if random.random() > 0.6:
                t = randtime() + timedelta(hours=1)
                for trace in traces:
                    trace['vessel_id'] = vessel.id
                    trace['time'] = time.mktime(t.timetuple())
                    t += timedelta(seconds=12)
                json.dump(traces, future)
                future.write('\n');
            else:
                for trace in traces:
                    p = Position(lat=trace['latitude'], lng=trace['longitude'])
                    p.save()

                    m = MovementLog()
                    m.vessel = vessel
                    m.time = t
                    m.position = p
                    m.zone_id = trace['zone_id']
                    m.bearing = trace['bearing']
                    m.speed = trace['speed']
                    m.save()

                    t += timedelta(seconds=12)

        logs = MovementLog.objects.all().order_by('time')

        currentZones = {}

        plan = Plan.objects.get(id=1)

        self.stdout.write('Generating plan...')
        for log in logs:
            # Get a random number between -1 and 1
            randN = (random.random()*2)-1
            timeDelta = timedelta(0, 12 * randN)

            # Check if we already know about this vessel
            if log.vessel in currentZones:
                # We do, if it's still in the same zone then ignore it
                if currentZones[log.vessel] == log.zone:
                    # Ignore
                    pass
                else:
                    # It has moved, add a new entry
                    planTime = log.time + timeDelta
                    PlanItem(plan=plan, time=planTime, vessel=log.vessel, zone=log.zone).save()
                    currentZones[log.vessel] = log.zone
            else:
                # It's a new vessel, track it and add a new entry
                planTime = log.time + timeDelta
                PlanItem(plan=plan, time=planTime, vessel=log.vessel, zone=log.zone).save()
                currentZones[log.vessel] = log.zone
    """

import json, os
from django.http import HttpResponse
from django.template import Context, loader
from django.db import connection
from datetime import datetime, date, timedelta
from Shimmer.rest_framework import ModelHandler
import models

def mkview(name):
    def view(context):
        template = loader.get_template(name + '.html')
        return HttpResponse(template.render(Context({'name': name})))
    return view

overview = mkview('overview')
incidents = mkview('incidents')
analysis = mkview('analysis')
report = mkview('report')

def status(request):
    if models.Berth.objects.get(pk=4).lastStatus.statusType == 'Error':
        template = loader.get_template('status2.html')
    elif models.MovementLog.objects.count() == 480:
        template = loader.get_template('status3.html')
    else:
        template = loader.get_template('status.html')
    return HttpResponse(template.render(Context({'name': 'status'})))

def query(request):
    cursor = connection.cursor()
    cursor.execute(request.GET['q'])
    colnames = [col[0] for col in cursor.description]
    data = [dict(zip(colnames, row)) for row in cursor.fetchall()]
    return HttpResponse(json.dumps(data))

def warn(request):
    if os.path.isfile('/home/will/Documents/GPIG_code/analysis/warn'):
        res = json.dumps({'warn': 1})
    else:
        res = json.dumps({'warn': 0})
    return HttpResponse(res)

class VesselHandler(ModelHandler):
    model = models.Vessel

class VesselDetailsHandler(ModelHandler):
    model = models.VesselDetails

class CargoTypeHandler(ModelHandler):
    model = models.CargoType

class PlanHandler(ModelHandler):
    model = models.Plan

class PlanItemHandler(ModelHandler):
    model = models.PlanItem

class PlanAlterationHandler(ModelHandler):
    model = models.PlanAlteration

class MovementLogHandler(ModelHandler):
    model = models.MovementLog

    def read(self, request, pk):
        """
            Read all objects of a model, or an individual one with the
            given primary key
        """
        if pk is None:
            query = self.model.objects.all()
        else:
            query = self.model.objects.filter(vessel_id=pk)
        if 'startTimestamp' in request.GET:
            query = query.filter(time__gte=datetime.fromtimestamp(int(request.GET['startTimestamp'])))
        return query.order_by('-time')

    def create(self, request, pk):
        p = models.Position(lat=request.data['latitude'],lng=request.data['longitude'])
        p.save()
        del request.data['latitude']
        del request.data['longitude']

        self._object_update(self.model(position=p), request.data)

class LaneZoneHandler(ModelHandler):
    model = models.LaneZone

    def create(self, request, pk):
        if pk is not None:
            raise NotImplemented('POST')

        dimensions = request.data['dimensions']
        del request.data['dimensions']

        obj = self.model()
        self._object_update(obj, request.data)
        for dim in dimensions:
            models.ZonePosition(zone=obj, lat=dim[0], lng=dim[1]).save()

class ShipModelHandler(ModelHandler):
    model = models.ShipModel

class BerthHandler(ModelHandler):
    model = models.Berth

class WeatherHandler(ModelHandler):
    model = models.Weather

class LaneZoneStatusHandler(ModelHandler):
    model = models.LaneZoneStatus

    def read(self, request, pk):
        """
            If a key is given, it refers to the laneZone, not the
            status id
        """
        if pk is None:
            query = self.model.objects.all()
        else:
            query = self.model.objects.filter(laneZone_id=pk)
        if 'startTimestamp' in request.GET:
            query = query.filter(lastChecked__gte=datetime.fromtimestamp(int(request.GET['startTimestamp'])))
        return query.order_by('-lastChecked')

class BerthStatusHandler(ModelHandler):
    model = models.BerthStatus

    def read(self, request, pk):
        """
            If a key is given, it refers to the berth, not the
            status id
        """
        if pk is None:
            query = self.model.objects.all()
        else:
            query = self.model.objects.filter(berth_id=pk)
        if 'startTimestamp' in request.GET:
            query = query.filter(lastChecked__gte=datetime.fromtimestamp(int(request.GET['startTimestamp'])))
        return query.order_by('-lastChecked')

class ShippingLaneHandler(ModelHandler):
    model = models.ShippingLane

class IncidentHandler(ModelHandler):
    model = models.Incident

class MooringLogHandler(ModelHandler):
    model = models.MooringLog
